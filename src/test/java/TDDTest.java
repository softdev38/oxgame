/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author natthawadee
 */
public class TDDTest {
    
    public TDDTest() {
    }
@Test
public void testAdd_1_2is3(){
    assertEquals(3,Example.add(1,2));
}
@Test
public void testAdd_3_4is7(){
   assertEquals(7,Example.add(3,4));
}
@Test
public void testAdd_20_22is42(){
   assertEquals(42,Example.add(20,22));
}
@Test
public void testChub_p1_p_p2_p_is_draw(){
    assertEquals("draw",Example.chub('p','p'));
}
@Test
public void testChub_p1_h_p2_h_is_draw(){
    assertEquals("draw",Example.chub('h','h'));
}
@Test
public void testChub_p1_s_p2_s_is_draw(){
    assertEquals("draw",Example.chub('s','s'));
}
@Test
public void testChub_p1_s_p2_p_is_p1(){
    assertEquals("p1",Example.chub('s','p'));
}
@Test
public void testChub_p1_h_p2_s_is_p1(){
    assertEquals("p1",Example.chub('h','s'));
}
@Test
public void testChub_p1_p_p2_h_is_p1(){
    assertEquals("p1",Example.chub('p','h'));
}
@Test
public void testChub_p1_h_p2_p_is_p2(){
    assertEquals("p2",Example.chub('h','p'));
}
@Test
public void testChub_p1_p_p2_s_is_p2(){
    assertEquals("p2",Example.chub('p','s'));
}
@Test
public void testChub_p1_s_p2_h_is_p2(){
    assertEquals("p2",Example.chub('s','h'));
}
}
