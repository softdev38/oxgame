

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author natthawadee
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @Test
    public void testCheckVerticlePlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                {'O', '-', '-'},
                                {'O', '-', '-'}};
        char currentPlayer = 'O';
        int  col = 1;
        assertEquals(true, OXProgram.checkVertical(table,currentPlayer,col));
    }
@Test
    public void testCheckVerticlePlayerOCol2NoWin() {
        char table[][] = {{'-', 'O', '-'}, 
                                {'-', 'O', '-'},
                                {'-', '-', '-'}};
        char currentPlayer = 'O';
        int  col = 2;
        assertEquals(false, OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckVerticlePlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                {'-', 'O', '-'},
                                {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int  col = 2;
        int row =1;
        assertEquals(true, OXProgram.checkWin(table,currentPlayer,row,col));
    }
}
